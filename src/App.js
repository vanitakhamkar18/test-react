import React from 'react';
// import './App.css';
import UserApp from './container/Userapp';

function App() {
    return (
        <div className="App">
          <UserApp />
        </div>
    );
}

export default App;
