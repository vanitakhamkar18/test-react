import React from 'react';
import { Collapsible, CollapsibleItem ,Icon} from 'react-materialize';

const user = ({users,accounts}) =>{
    console.log(accounts);
    return(

    <Collapsible accordion popout>
        {
            users && Object.keys(users).map(function(key,user) {
                 console.log(accounts[users[key].account]);
                var app = accounts[users[key].account];
                return(
                    <CollapsibleItem key={key}
                    expanded={false}
                    header={users[key].name}
                    icon={<Icon>add</Icon>}
                    node="div"
                    >
                    {
                        app && Object.keys(app).map(function(ke,index) {
                           console.log(app[ke]);
                           var appTitle = app[ke];
                           return(
                                <div  key={ke}>
                                Account ID : {
                                        users[key].account
                                }
                                {

                                    appTitle && Object.keys(appTitle).map(function(k,i) {
                                       return(
                                            <div key={k}>APP Title : {appTitle[k].title}</div>
                                        )
                                    })
                                }
                                </div>
                            );
                        })
                    }
                    </CollapsibleItem>
                )
            })
        }

    </Collapsible>
);

}

export default user;
