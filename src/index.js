import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import { createStore ,applyMiddleware, compose } from 'redux';
import reducer from './store/reducers/reducer';
import * as serviceWorker from './serviceWorker';
import thunk from 'redux-thunk';
import { reduxFirestore , getFirestore } from 'redux-firestore';
import { reactReduxFirebase , getFirebase } from 'react-redux-firebase';
import fbConfig from './config/fbconfig';
import firebase from 'firebase/app';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const rrfConfig = {
  userProfile: 'users'
  // useFirestoreForProfile: true // Firestore for Profile instead of Realtime DB
}
//const store = createStore(reducer,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

const store = createStore(reducer, composeEnhancers(
    applyMiddleware(thunk.withExtraArgument({ getFirebase , getFirestore })),
    reduxFirestore(fbConfig),
    reactReduxFirebase(fbConfig)
    ));

ReactDOM.render(
  <React.StrictMode>
  <Provider store={store}>
    <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
