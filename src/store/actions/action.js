export const FETCH_USER = 'FETCH_USER';
export const FETCH_ACCOUNT = 'FETCH_ACCOUNT';
export const FETCH_DATA_FAILED = 'FETCH_DATA_FAILED';
