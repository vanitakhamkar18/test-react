import * as actionTypes from './action';
import axios from 'axios';


export const setAccount = ( accounts ) => {
    return {
        type: actionTypes.FETCH_ACCOUNT,
        accounts: accounts
    };
};

export const fetchDataFailed = () => {
    return {
        type: actionTypes.FETCH_DATA_FAILED
    };
};

export const initAccountData = () => {
    return dispatch => {
        axios.get('https://test-vani.firebaseio.com/accounts.json')
            .then( response => {
               dispatch(setAccount(response.data));
            } )
            .catch( error => {
                dispatch(fetchDataFailed());
            } );
    };
};
