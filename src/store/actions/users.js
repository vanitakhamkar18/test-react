import * as actionTypes from './action';
import axios from 'axios';


export const setUser = ( users ) => {
    return {
        type: actionTypes.FETCH_USER,
        users: users
    };
};

export const fetchDataFailed = () => {
    return {
        type: actionTypes.FETCH_DATA_FAILED
    };
};

export const initUserData = () => {
    return (dispatch,getState,{ getFirebase, getFirestore }) => {
        const firestore =  getFirestore();
        const coll = firestore.collection('test-vani');
        //console.log(coll);
        axios.get('https://test-vani.firebaseio.com/users.json')
            .then( response => {
               dispatch(setUser(response.data));
            } )
            .catch( error => {
                dispatch(fetchDataFailed());
            } );
    };
};
