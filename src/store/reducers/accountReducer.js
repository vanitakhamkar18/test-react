import * as actionTypes from '../actions/action';

const initialState = {
    accounts :
    {

        "-Kd_teAAXcw2b5MyFPIT": {
          "apps": {
            "cuckoosnest": {
              "title": "One Flew Over The Cuckoo’s Nest"
            }
          }
        },
        "-Kd_ZCjRYSGzISxY_5Wi": {
          "apps": {
            "psycho": {
              "title": "Psycho"
            }
          }
        }

    },
    error : false
};



const accountReducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.FETCH_ACCOUNT:
            return {
                ...state,
                accounts:action.accounts,
                error: false
            };

        case actionTypes.FETCH_DATA_FAILED:
            return {
                ...state,
                error: true
            };
        default:
            return state;
    }
    return state;

};

export default accountReducer;
