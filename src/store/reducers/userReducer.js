import * as actionTypes from '../actions/action';

const initialState = {
    users:
    {
        "00L91c7cvUaghNmGlC0lJa9eZ102": {
          "account": "-Kd_teAAXcw2b5MyFPIT",
          "name": "Randle McMurphy"
        },
        "0YRaZC6EUrc5sc8Ab4AR7Zp7ig93": {
          "account": "-Kd_ZCjRYSGzISxY_5Wi",
          "name": "Norman Bates"
        }
    },
    error : false

};



const userReducer = ( state = initialState, action ) => {
    console.log(action);
    switch ( action.type ) {
        case actionTypes.FETCH_USER:
            return {
                ...state,
                users:action.users,
                error: false
            };

        case actionTypes.FETCH_DATA_FAILED:
            return {
                ...state,
                error: true
            };
        default:
            return state;
    }
    return state;

};

export default userReducer;
