import * as actionTypes from '../actions/action';
import userReducer  from './userReducer';
import accountReducer  from './accountReducer';
import { combineReducers } from 'redux';
import { firestorereducer } from 'redux-firestore';

const reducer = combineReducers({
    user : userReducer,
    account : accountReducer,
    // firestore : firestorereducer
});
export default reducer;
