import React, { Component } from 'react';

// import Order from '../../components/Order/Order';
import User from '../component/User';
import Header from '../component/Header';
import { connect } from 'react-redux';
import * as userAccountAction from '../store/actions/index';
// import {firestoreConnect} from 'react-redux-firebase';
import {compose} from 'redux';
class Userapp extends Component {
    state = {
        loading: true
    }

    componentDidMount() {
        console.log(this.props);
        this.props.onInitData();
    }

    render () {
        const { users,accounts,error } = this.props;
        let errorModal = (<div>Opps Something went wrong..!!
                            <div class="progress"><div class="indeterminate"></div></div>
                        </div>
                        );
        let modal = error == false ?
                    (
                    <User users={users} accounts={accounts} />)
                : errorModal;
        return (
            <div className="row center-align">
                <div className="col s12 m8 hoverable ">
                <Header />
                {modal}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    console.log(state)
    return {
        users : state.user.users,
        accounts : state.account.accounts,
        error : state.account.error
    }
}



const mapDispatchToProps = dispatch => {
    return {
        onInitData: () => {
            dispatch(userAccountAction.initAccountData());
            dispatch(userAccountAction.initUserData());
        }
    }
}

export default compose(
connect(mapStateToProps,mapDispatchToProps),
// firestoreConnect({
//     collection : 'accounts'
// })
)
(Userapp);
